use std::fmt::Debug;

use reqwest::{header::HeaderValue, RequestBuilder};

use crate::api::{
    endpoints::Endpoint, Achievement, AllAccountAchievements, AllAchievementIDs, Dailies,
};

type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    MissingApiKey,
    Request(reqwest::Error),
    Json(serde_json::Error),
    InvalidHeaderValue(reqwest::header::InvalidHeaderValue),
}

/// A client for making requests to a Guild Wars 2 API gateway
pub struct GW2Client {
    client: reqwest::Client,
    gateway: String,
    apikey: HeaderValue,
}

impl GW2Client {
    pub fn new(gateway: String, apikey: String) -> Result<Self> {
        Ok(Self {
            client: reqwest::Client::new(),
            gateway,
            apikey: format!("Bearer {}", apikey)
                .parse()
                .map_err(Error::InvalidHeaderValue)?,
        })
    }

    /// Request [AllAchievementIDs] from gateway
    pub async fn achievement_ids(&self) -> Result<AllAchievementIDs> {
        self.request_one::<AllAchievementIDs>().await
    }

    /// Request [Achievement]'s from gateway
    pub async fn achievements(&self, ids: Vec<usize>) -> Result<Vec<Achievement>> {
        self.request_many::<Achievement>(ids).await
    }

    /// Request [AllAccountachievements] from gateway
    pub async fn all_account_achievements(&self) -> Result<AllAccountAchievements> {
        self.request_one::<AllAccountAchievements>().await
    }

    /// Request [Dailies] from gateway
    pub async fn dailies(&self) -> Result<Dailies> {
        self.request_one::<Dailies>().await
    }

    /// Make a request for a single [Endpoint]
    async fn request_one<E: Endpoint>(&self) -> Result<E> {
        let response = self.request::<E>(vec![]).await?;
        serde_json::from_str::<E>(&response).map_err(Error::Json)
    }

    /// Make a request for a list of [Endpoint]'s from a list of parameters
    async fn request_many<E: Endpoint>(&self, params: Vec<E::Parameter>) -> Result<Vec<E>> {
        if params.is_empty() {
            return Ok(vec![]);
        }

        let response = self.request::<E>(params).await?;
        serde_json::from_str::<Vec<E>>(&response).map_err(Error::Json)
    }

    /// Make send a request and get the response body's text
    async fn request<E: Endpoint>(&self, params: Vec<E::Parameter>) -> Result<String> {
        self.build_request::<E>(&params)?
            .send()
            .await
            .map_err(Error::Request)?
            .text()
            .await
            .map_err(Error::Request)
    }

    /// Prepare a request to be sent
    fn build_request<E: Endpoint>(&self, params: &Vec<E::Parameter>) -> Result<RequestBuilder> {
        Ok(self
            .client
            .get(E::get_path(&self.gateway, params))
            .headers(E::headers(self.apikey.clone())?))
    }
}
