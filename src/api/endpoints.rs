use reqwest::header::{HeaderMap, HeaderValue};
use serde::de::DeserializeOwned;

use crate::client;

use super::{AccountAchievement, Achievement, AllAccountAchievements, AllAchievementIDs, Dailies};

type Result<T> = std::result::Result<T, client::Error>;

/// Represents how and where to access the requested data
pub trait Endpoint: DeserializeOwned {
    type Parameter;

    /// Build a url path to the endpoint from the provided parameters
    fn get_path(gateway: &str, param: &Vec<Self::Parameter>) -> String;

    fn headers(apikey: HeaderValue) -> Result<HeaderMap>;
}

impl Endpoint for AllAchievementIDs {
    type Parameter = ();

    fn get_path(gateway: &str, _: &Vec<()>) -> String {
        format!("{}/v2/achievements", gateway)
    }

    fn headers(_apikey: HeaderValue) -> Result<HeaderMap> {
        Ok(HeaderMap::default())
    }
}

impl Endpoint for Achievement {
    type Parameter = usize;

    fn get_path(gateway: &str, ids: &Vec<usize>) -> String {
        format!(
            "{}/v2/achievements?ids={}",
            gateway,
            ids.iter()
                .map(|id| id.to_string())
                .collect::<Vec<String>>()
                .join(",")
        )
    }

    fn headers(_apikey: HeaderValue) -> Result<HeaderMap> {
        Ok(HeaderMap::default())
    }
}

impl Endpoint for AccountAchievement {
    type Parameter = usize;

    fn get_path(gateway: &str, ids: &Vec<usize>) -> String {
        format!(
            "{}/v2/account/achievements?ids={}",
            gateway,
            ids.iter()
                .map(|id| id.to_string())
                .collect::<Vec<String>>()
                .join(",")
        )
    }

    fn headers(apikey: HeaderValue) -> Result<HeaderMap> {
        let mut headermap = HeaderMap::new();
        headermap.insert("Authorization", apikey);
        Ok(headermap)
    }
}

impl Endpoint for AllAccountAchievements {
    type Parameter = ();

    fn get_path(gateway: &str, _: &Vec<()>) -> String {
        format!("{}/v2/account/achievements", gateway)
    }

    fn headers(apikey: HeaderValue) -> Result<HeaderMap> {
        let mut headermap = HeaderMap::new();
        headermap.insert("Authorization", apikey);
        Ok(headermap)
    }
}

impl Endpoint for Dailies {
    type Parameter = ();

    fn get_path(gateway: &str, _: &Vec<()>) -> String {
        format!("{}/v2/achievements/daily", gateway)
    }

    fn headers(_apikey: HeaderValue) -> Result<HeaderMap> {
        let mut headermap = HeaderMap::new();
        headermap.insert(
            "X-Schema-Version",
            "2019-05-16T00:00:00.000Z"
                .parse()
                .map_err(client::Error::InvalidHeaderValue)?,
        );
        Ok(headermap)
    }
}
